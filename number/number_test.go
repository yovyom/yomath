package number

import (
	"fmt"
	"testing"
)

func TestLCMSNumber(t *testing.T) {
	actual := LCM(10, 12)
	expected := 60
	if actual != expected {
		t.Errorf("Test Failure: Expected '%v', Actual: %v\n", expected, actual)
	}
}

func TestGCDNumber(t *testing.T) {
	actual := GCD(10, 12)
	expected := 2
	if actual != expected {
		t.Errorf("Test Failure: Expected '%v', Actual: %v\n", expected, actual)
	}
}

func TestPrimeNumber(t *testing.T) {
	inputs := []struct {
		n        int
		expected bool
		e        error
	}{
		{n: 1, expected: false, e: fmt.Errorf("number has to be greater than 1")},
		{n: 100, expected: false, e: nil},
		{n: 2, expected: true, e: nil},
		{n: -10, expected: false, e: fmt.Errorf("number has to be greater than 1")},
		{n: 101, expected: true, e: nil},
		{n: 103, expected: true, e: nil},
		{n: 29, expected: true, e: nil},
		{n: 281, expected: true, e: nil},
		// 29*281
		{n: 8193, expected: false, e: nil},
		{n: 100237, expected: true, e: nil},
		{n: 999953, expected: true, e: nil},
	}
	for _, input := range inputs {
		actual, err := IsPrime(input.n)
		if err == nil {
			if actual != input.expected {
				t.Errorf("Test Failure: Number '%v' Expected '%v', Actual: '%v'\n", input.n, input.expected, actual)
			}
		} else {
			if err.Error() != input.e.Error() {
				t.Errorf("Test Failure: Number '%v' Expected '%v', Actual: '%v'\n", input.n, input.e, err)
			}
		}
	}
}
