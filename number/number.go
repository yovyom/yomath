package number

import "fmt"

func GCD(n1, n2 int) int {
	return gcd(n1, n2)
}

func gcd(n1, n2 int) int {
	for n1%n2 != 0 {
		n1, n2 = n2, n1%n2
	}
	return n2
}

func LCM(n1, n2 int) int {
	return n1 * n2 / gcd(n1, n2)
}

func IsPrime(n int) (bool, error) {
	if n < 2 {
		return false, fmt.Errorf("number has to be greater than 1")
	}
	for i := 2; i < n/2; i++ {
		if n%i == 0 {
			return false, nil
		}
	}
	return true, nil
}
