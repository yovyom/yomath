package string

import "fmt"

const encoding = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
const padChar = '='

var encodingReverse map[byte]byte

func init() {
	encodingReverse = make(map[byte]byte)
	for i := 0; i < len(encoding); i++ {
		encodingReverse[encoding[i]] = byte(i)
	}
}

// base64 encodes 3 bytes at a time of src bytes into 4 bytes
// If length of source bytes is not exact multiple of 3,
// 2 bytes are padded when [len(src)%3 == 1]  and
// 1 byte is padded when [len(src)%3 == 2]
func Base64Encode(src []byte) (dst []byte) {
	n := (len(src) + 2) / 3
	dst = make([]byte, n*4)
	pad := 3*n - len(src)
	for i := 0; i < n; i++ {
		if i == n-1 {
			base64Encode(src, dst, i, pad)
		} else {
			base64Encode(src, dst, i, 0)
		}
	}
	return dst
}

func Base64EncodeToString(src []byte) string {
	dst := Base64Encode(src)
	return string(dst)
}

func base64Encode(src, dst []byte, index, pad int) {
	srcIndex, dstIndex := index*3, index*4
	if pad == 0 {
		convert3To4(src, dst, srcIndex, dstIndex)
	} else {
		srcPad := make([]byte, 3)
		srcPad[0] = src[srcIndex]
		if pad == 1 {
			srcPad[1] = src[srcIndex+1]
		}
		convert3To4(srcPad, dst, 0, dstIndex)
		switch pad {
		case 1:
			dst[dstIndex+3] = padChar
		case 2:
			dst[dstIndex+2] = padChar
			dst[dstIndex+3] = padChar
		}
	}
}

func convert3To4(src, dst []byte, srcIndex, dstIndex int) {
	dst[dstIndex] = encoding[src[srcIndex]>>2]
	dst[dstIndex+1] = encoding[(src[srcIndex]&3)<<4+src[srcIndex+1]>>4]
	dst[dstIndex+2] = encoding[(src[srcIndex+1]&15)<<2+src[srcIndex+2]>>6]
	dst[dstIndex+3] = encoding[src[srcIndex+2]&63]
}

func Base64Decode(src []byte) (dst []byte, err error) {
	srcLen := len(src)
	if srcLen < 4 || srcLen%4 != 0 {
		return dst, fmt.Errorf("byte array to decode is not of valid length: %v", srcLen)
	}

	pad := 0
	if src[srcLen-2] == padChar {
		if src[srcLen-1] == padChar {
			pad = 2
		} else {
			return dst, fmt.Errorf("wrong padding")
		}
	} else if src[srcLen-1] == padChar {
		pad = 1
	}

	n := srcLen / 4
	dst = make([]byte, n*3-pad)
	for i := 0; i < n; i++ {
		if i == n-1 {
			err = base64Decode(src, dst, i, pad)
		} else {
			err = base64Decode(src, dst, i, 0)
		}
		if err != nil {
			return dst, err
		}
	}
	return dst, nil
}

func base64Decode(src, dst []byte, index, pad int) error {
	srcIndex, dstIndex := index*4, index*3
	return convert4To3(src, dst, srcIndex, dstIndex, pad)
}

// Converts 4 encoded Base64 characters to 3 bytes
// When
// padding = 2
//   String we are tring to decode is of the form YQ==,
//   result of encoding just one byte
// padding = 1, need to return only the two values
//   String we are tring to decode is of the form YWI=,
//   result of encoding two bytes
// padding = 0
//   String we are trying to decode is result of encoding
//   3 bytes
func convert4To3(src, dst []byte, srcIndex, dstIndex int, pad int) error {
	x := make([]byte, 4)
	for i := 0; i < 4-pad; i++ {
		if key, ok := encodingReverse[src[srcIndex+i]]; !ok {
			return fmt.Errorf("invalid character '%c' in encoded message", src[srcIndex+i])
		} else {
			x[i] = key
		}
	}
	dst[dstIndex] = x[0]<<2 + x[1]>>4&3
	switch pad {
	case 0:
		dst[dstIndex+1] = x[1]<<4 + x[2]>>2&15
		dst[dstIndex+2] = x[2]<<6 + x[3]&63
	case 1:
		dst[dstIndex+1] = x[1]<<4 + x[2]>>2&15
	}

	return nil
}

func Base64DecodeToString(src []byte) (dst string, err error) {
	dstBytes, err := Base64Decode(src)
	if err != nil {
		return dst, err
	}
	return string(dstBytes), err
}
