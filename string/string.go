package string

import (
	"strings"
)

// Least common multiple or LCM of two strings s1 and s2 is a string of
// the smallest possible length which completely contains s1 and s2.
// LCM("yogi", "ayo") in terms of the result containing both the strings can be
// "ayogi", "yogiayo" or "ayoyogi" (last two concatenation of two strings) or "xxxayogi".
// But "ayogi" has the smallest possible length and is the correct answer.
// For LCM("ab", "cd"), both "abcd" or "cdba" are acceptable answers as both have length
// of 4 which is the smallest possible length of a string which completely
// contains "ab" and "cd"
// LCM("yogi", "mogi") = "yogimogi" or "mogiyogi"
// LCM("automation", "automationedge") = "automationedge"
// LCM("abcX", "Xabc") = "XabcX"

func LCM(s1, s2 string) string {
	lcm1 := lcm(s1, s2)
	lcm2 := lcm(s2, s1)
	if len(lcm1) < len(lcm2) {
		return lcm1
	} else {
		return lcm2
	}
}

func lcm(s1, s2 string) string {
	for i := 0; i < len(s1); i++ {
		prefix := s1[i:]
		if strings.HasPrefix(s2, prefix) {
			return s1[0:i] + s2
		}
	}
	return s1 + s2
}

// GCD of two strings s1 and s2 is a string of the largest possible
// length which is completely contained in both s1 and s2.
// GCD("yogi", "mogi") = "ogi"
// GCD("abc", "abcd") = "abc"
// GCD("same", "same") = "same"
// GCD("xabc", "abcy") = "abc"
// GCD("abc", "xyabzbc") = "ab" or "bc"
// GCD("x", "y") = ""

func GCD(s1, s2 string) string {
	// function "gcd" would do its job no matter if
	// s1 is longer or s2. If s1 and s2 are more or
	// less of similar lenght, this wouldn't matter
	// But if one is much shorter than other,
	// computation happens much efficiently
	if len(s1) <= len(s2) {
		return gcd(s1, s2)
	} else {
		return gcd(s2, s1)
	}
}

func gcd(s1, s2 string) string {
	gcd := ""
	// forming substring of s1 of the type
	// s1[0:], s1[1:], s1[2:], ....
	for i := 0; i < len(s1); i++ {
		ss := s1[i:]
		if strings.Contains(s2, ss) {
			gcd = ss
			break
		}
	}

	// forming substring of s1 of the type
	// s1[0:len-1], s1[0:len-2], s1[0:len-3], ....
	// s1[0:len] is already covered in the previous block
	for i := 0; i < len(s1)-1; i++ {
		ss := s1[0 : len(s1)-1-i]
		if strings.Contains(s2, ss) {
			if len(ss) > len(gcd) {
				gcd = ss
			}
			break
		}
	}

	return gcd
}
