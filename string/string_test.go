package string

import (
	b64 "encoding/base64"
	"fmt"
	"testing"
)

func TestLCMString(t *testing.T) {
	t.Skip()
	actual := LCM("automation", "automationedge")
	expected := "automationedge"
	if actual != expected {
		t.Errorf("Test Failure: Expected '%v', Actual: %v\n", expected, actual)
	}

	actual = LCM("yogimogi", "yogi")
	expected = "yogimogi"
	if actual != expected {
		t.Errorf("Test Failure: Expected '%v', Actual: %v\n", expected, actual)
	}
}

func TestLCMStringTableDriven(t *testing.T) {
	inputs := []struct {
		s1       string
		s2       string
		expected []string
	}{
		{s1: "automation", s2: "automationedge", expected: []string{"automationedge"}},
		{s1: "yogimogi", s2: "yogi", expected: []string{"yogimogi"}},
		{s1: "abcX", s2: "Xabc", expected: []string{"XabcX"}},
		{s1: "abcX", s2: "Xyz", expected: []string{"abcXyz"}},
		{s1: "ab", s2: "cd", expected: []string{"abcd", "cdab"}},
		{s1: "abc", s2: "xbcy", expected: []string{"abcxbcy", "xbcyabc"}},
		{s1: "", s2: "YO", expected: []string{"YO"}},
		{s1: "", s2: "", expected: []string{""}},
	}
	for _, input := range inputs {
		actual := LCM(input.s1, input.s2)
		pass := false
		for _, e := range input.expected {
			if actual == e {
				pass = true
				break
			}
		}
		if !pass {
			t.Errorf("Test Failure, s1: '%v', s2: '%v', Expected '%v', Actual: %v\n",
				input.s1, input.s2, input.expected, actual)
		}
	}
}

func TestGCDString(t *testing.T) {
	actual := GCD("x", "y")
	expected := ""
	if actual != expected {
		t.Errorf("Test Failure: Expected '%v', Actual: %v\n", expected, actual)
	}

	actual = GCD("abcX", "abc")
	expected = "abc"
	if actual != expected {
		t.Errorf("Test Failure: Expected '%v', Actual: %v\n", expected, actual)
	}

	actual = GCD("hello", "1hello2")
	expected = "hello"
	if actual != expected {
		t.Errorf("Test Failure: Expected '%v', Actual: %v\n", expected, actual)
	}

	actual = GCD("abc", "YoabX")
	expected = "ab"
	if actual != expected {
		t.Errorf("Test Failure: Expected '%v', Actual: %v\n", expected, actual)
	}
}

func TestBase64Encode(t *testing.T) {
	inputs := []struct {
		in       string
		expected string
	}{
		{in: "", expected: ""},
		{in: "a", expected: "YQ=="},
		{in: "ab", expected: "YWI="},
		{in: "abc", expected: "YWJj"},
		{in: "asfdsfdsfsdfsdfsdfdsfsdfsda", expected: "YXNmZHNmZHNmc2Rmc2Rmc2RmZHNmc2Rmc2Rh"},
		{in: "abc123!?$*&()'-=@~", expected: "YWJjMTIzIT8kKiYoKSctPUB+"},
		{in: " dog.", expected: "IGRvZy4="},
		{in: "xx dog.", expected: "eHggZG9nLg=="},
		{in: "The quick brown fox jumps over the lazy dog.", expected: "VGhlIHF1aWNrIGJyb3duIGZveCBqdW1wcyBvdmVyIHRoZSBsYXp5IGRvZy4="},
	}
	for _, input := range inputs {
		actual := Base64EncodeToString([]byte(input.in))
		if actual != input.expected {
			t.Errorf("Test Failure: Expected '%v', Actual: '%v'\n", input.expected, actual)
		}
	}
}

func TestBase64Decode(t *testing.T) {
	inputs := []struct {
		in       string
		expected string
		err      error
	}{
		{in: "YWJj", expected: "abc", err: nil},
		{in: "YQ==", expected: "a", err: nil},
		{in: "YQ=A", expected: "", err: fmt.Errorf("wrong padding")},
		{in: "YWI=", expected: "ab"},
		{in: "YWI=A", expected: "", err: fmt.Errorf("byte array to decode is not of valid length: 5")},
		{in: "YXNmZHNmZHNmc2Rmc2Rmc2RmZHNmc2Rmc2Rh", expected: "asfdsfdsfsdfsdfsdfdsfsdfsda"},
		{in: "YWJjMTIzIT8kKiYoKSctPUB+", expected: "abc123!?$*&()'-=@~"},
		{in: "IGRvZy4=", expected: " dog."},
		// You can have at most 2 padding characters, third-last character '=' in below encoded string
		// is not one of the 64 symbols used by Base64 encoding
		{in: "eHggZG9nL===", expected: "", err: fmt.Errorf("invalid character '=' in encoded message")},
		{in: "eHggZG9nLg==", expected: "xx dog."},
		{in: "HEMANANDTHEMASTERSOFTHE", expected: "ab", err: fmt.Errorf("byte array to decode is not of valid length: 23")},
		{in: "VGhlIHF1aWNrIGJyb3duIGZveCBqdW1wcyBvdmVyIHRoZSBsYXp5IGRvZy4=", expected: "The quick brown fox jumps over the lazy dog."},
		{in: "?HggZG9nLg==", expected: "", err: fmt.Errorf("invalid character '?' in encoded message")},
	}
	for _, input := range inputs {
		actual, err := Base64DecodeToString([]byte(input.in))
		if err == nil {
			if actual != input.expected {
				t.Errorf("Test Failure: Expected '%v', Actual: '%v'\n", input.expected, actual)
			}
		} else {
			if err.Error() != input.err.Error() {
				t.Errorf("Test Failure: Expected '%v', Actual: '%v'\n", input.err, err)
			}
		}
	}
}

// const base64Decoded = "The quick brown fox jumps over the lazy dog..The quick brown fox jumps over the lazy dog..The quick brown fox jumps over the lazy dog..The quick brown fox jumps over the lazy dog..The quick brown fox jumps over the lazy dog..The quick brown fox jumps over the lazy dog..The quick brown fox jumps over the lazy dog..The quick brown fox jumps over the lazy dog..The quick brown fox jumps over the lazy dog..The quick brown fox jumps over the lazy dog.."
// const base64Encoded = "VGhlIHF1aWNrIGJyb3duIGZveCBqdW1wcyBvdmVyIHRoZSBsYXp5IGRvZy4uVGhlIHF1aWNrIGJyb3duIGZveCBqdW1wcyBvdmVyIHRoZSBsYXp5IGRvZy4uVGhlIHF1aWNrIGJyb3duIGZveCBqdW1wcyBvdmVyIHRoZSBsYXp5IGRvZy4uVGhlIHF1aWNrIGJyb3duIGZveCBqdW1wcyBvdmVyIHRoZSBsYXp5IGRvZy4uVGhlIHF1aWNrIGJyb3duIGZveCBqdW1wcyBvdmVyIHRoZSBsYXp5IGRvZy4uVGhlIHF1aWNrIGJyb3duIGZveCBqdW1wcyBvdmVyIHRoZSBsYXp5IGRvZy4uVGhlIHF1aWNrIGJyb3duIGZveCBqdW1wcyBvdmVyIHRoZSBsYXp5IGRvZy4uVGhlIHF1aWNrIGJyb3duIGZveCBqdW1wcyBvdmVyIHRoZSBsYXp5IGRvZy4uVGhlIHF1aWNrIGJyb3duIGZveCBqdW1wcyBvdmVyIHRoZSBsYXp5IGRvZy4uVGhlIHF1aWNrIGJyb3duIGZveCBqdW1wcyBvdmVyIHRoZSBsYXp5IGRvZy4u"

const base64Decoded = "abcabcabcabcabcabc"
const base64Encoded = "YWJjYWJjYWJjYWJjYWJjYWJj"

func BenchmarkBase64Encode(b *testing.B) {
	for i := 0; i < b.N; i++ {
		actual := Base64EncodeToString([]byte(base64Decoded))
		if actual != base64Encoded {
			b.Errorf("Test Failure: Expected '%v', Actual: '%v'\n", base64Encoded, actual)
		}
	}
}

func BenchmarkBase64EncodeStdLib(b *testing.B) {
	for i := 0; i < b.N; i++ {
		actual := b64.StdEncoding.EncodeToString([]byte(base64Decoded))
		if actual != base64Encoded {
			b.Errorf("Test Failure: Expected '%v', Actual: '%v'\n", base64Encoded, actual)
		}
	}
}

func BenchmarkBase64Decode(b *testing.B) {
	for i := 0; i < b.N; i++ {
		actual, _ := Base64DecodeToString([]byte(base64Encoded))
		if actual != base64Decoded {
			b.Errorf("Test Failure: Expected '%v', Actual: '%v'\n", base64Decoded, actual)
		}
	}
}
func BenchmarkBase64DecodeStdLib(b *testing.B) {
	for i := 0; i < b.N; i++ {
		actual, _ := b64.StdEncoding.DecodeString(base64Encoded)
		if string(actual) != base64Decoded {
			b.Errorf("Test Failure: Expected '%v', Actual: '%v'\n", base64Decoded, actual)
		}
	}
}
